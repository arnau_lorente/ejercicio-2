package com.dwandroid.arnaulorente.ejercicio_2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener {
	EditText et_usuario, et_password;
	Button bt_login, bt_salir;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		et_usuario = (EditText) findViewById(R.id.etUser);
		et_password = (EditText) findViewById(R.id.etPsw);
		bt_login = (Button) findViewById(R.id.btLogin);
		bt_salir = (Button) findViewById(R.id.btSalir);
		
		bt_login.setOnClickListener(this);
		bt_salir.setOnClickListener(this);
		
		}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btLogin:
				//Variables para obtener texto de los EditText, el texto lo convertimos en cadena
				String user_obtenido = et_usuario.getText().toString();
				String psw_obtenido = et_password.getText().toString();
				
				//Variables donde almacenamos los datos correctos				
				String user_correcto = "desarrolloweb";
				String psw_correcto = "#dwAndroid";
				
				//Comprobamos si estan vacios
				int casos = 0; // Variable auxiliar que nos ayuda a saber que opcion escogemos.
				
				if (user_obtenido.equals("") && psw_obtenido.equals("")) { 
					casos = 1; 
				} else {
					//Si no estan vacios, comprobamos si alguno de ellos est� vacio.
					if (user_obtenido.equals("")) { casos = 2; }
					if (psw_obtenido.equals("")) { casos = 3; }
				}
				
				//Comprobamos usuario y contrase�a son correctos
				if (user_obtenido.equals(user_correcto) && psw_obtenido.equals(psw_correcto)) { 
					casos = 4; 
				} else if (casos == 0) {
						casos = 5;
						if (!user_obtenido.equals(user_correcto) && psw_obtenido.equals(psw_correcto)) { casos = 6;}
						if (user_obtenido.equals(user_correcto) && !psw_obtenido.equals(psw_correcto)) { casos = 7;}
				}
			
				//Seleccion de los mensajes o acciones en funci�n de la opci�n.
				switch (casos) {
				case 1:
					Toast.makeText(this, "Usuario y contrase�a no pueden estar vacios", Toast.LENGTH_SHORT).show();
				break;
				case 2:
					Toast.makeText(this, "Usuario no puede estar vacio", Toast.LENGTH_SHORT).show();
				break;
				case 3:
					Toast.makeText(this, "Contrase�a no puede estar vacio", Toast.LENGTH_SHORT).show();
				break;
				case 4:
					Intent intent = new Intent("android.intent.action.ACTIVITYDOS"); //Creamos el objeto intent
					intent.putExtra("DATO", user_obtenido); //Le enviamos un paquete dentro del objeto intent.
					startActivity(intent); //Iniciamos el activity
				break;
				case 5:
					Toast.makeText(this, "Usuario y contrase�a incorrectos", Toast.LENGTH_SHORT).show();
				break;
				case 6:
					Toast.makeText(this, "Usuario incorrecto", Toast.LENGTH_SHORT).show();
				break;
				case 7:
					Toast.makeText(this, "Contrase�a incorrecta", Toast.LENGTH_SHORT).show();
				break;

				}
			break; //Fin case R.id.btLogin

			case R.id.btSalir:
				//Bot�n para finalizar la aplicaci�n, muestra mensaje mediante Toast
				String finalizar = "Aplicaci�n finalizada";
				Toast.makeText(this, finalizar, Toast.LENGTH_SHORT).show();
				finish(); //Finaliza la aplicaci�n.
			break;
		}
	}	

}	

		
		
	
	
	



