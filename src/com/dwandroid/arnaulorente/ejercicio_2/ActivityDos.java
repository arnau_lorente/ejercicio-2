package com.dwandroid.arnaulorente.ejercicio_2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityDos extends Activity {
	TextView mostrar_dato;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitydos);
		
		mostrar_dato = (TextView) findViewById(R.id.tv_mostrar);
			
		Bundle extras = getIntent().getExtras(); //Recibimos el dato del intent, y cogemos los extras
		String dato = extras.getString("DATO");
		mostrar_dato.setText("Bienvenido a la aplicación, "+dato);
		
		
		
		}
	
	
	
	
	
}
